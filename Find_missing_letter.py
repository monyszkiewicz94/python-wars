#Find the missing letter

# Write a method that takes an array of consecutive (increasing) letters as input and that returns the missing letter in the array.

# You will always get an valid array. And it will be always exactly one letter be missing. The length of the array will always be at least 2.
# The array will always contain letters in only one case.

# Example:

# ['a','b','c','d','f'] -> 'e'
# ['O','Q','R','S'] -> 'P'
# (Use the English alphabet with 26 letters!)




def find_missing_letter(chars):
    temp = 0
    for char in chars:
        if ord(char) - temp == 2:
            return chr(ord(char)-1)
        else:
            temp = ord(char)
        
# this one works too
# def find_missing_letter(chars):
#     list = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
#     list = list[list.find(chars[0]):int(list.find(chars[-1])+1)]
#     for letter in list:
#         if chars.count(letter):
#             continue
#         else:
#             return letter